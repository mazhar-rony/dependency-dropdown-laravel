<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\SubSubCategoryController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('home');
});

Route::resource('categories', CategoryController::class);

Route::get('categories/{category}/sub-categories', [CategoryController::class, 'getSubCategories'])->name('categories.sub-categories');

Route::resource('sub-categories', SubCategoryController::class);

Route::get('sub-categories/{sub_category}/sub-sub-categories', [SubCategoryController::class, 'getSubSubCategories'])->name('sub-categories.sub-sub-categories');

Route::resource('sub-sub-categories', SubSubCategoryController::class);

Route::resource('products', ProductController::class);


