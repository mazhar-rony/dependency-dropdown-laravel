<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'sub_category_id' => 'required|exists:sub_categories,id',
            'title' => 'required|max:255|min:3|unique:sub_sub_categories,title,' . $this->route('sub_sub_category')?->id,
        ];
    }
}
