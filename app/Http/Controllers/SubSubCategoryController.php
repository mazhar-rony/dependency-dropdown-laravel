<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Models\SubSubCategory;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\Http\Requests\SubSubCategoryRequest;

class SubSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subSubCategories = SubSubCategory::with('subCategory.category')->latest()->paginate(10);
      
        return view('sub_sub_categories.index', compact('subSubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sub_sub_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubSubCategoryRequest $request)
    {
        try {
            SubSubCategory::create([
                'sub_category_id' => $request->sub_category_id,
                'title' => $request->title,
                'is_active' => $request->is_active ?? 0,
            ]);
            return redirect()->route('sub-sub-categories.index')->withMessage('Successfully Created');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = SubSubCategory::findOrFail($id);
        $subCategories = $category->subCategory->where('id', $category->sub_category_id)->get()->toArray();

        // $subCategories = $category->subCategory->where('id', $category->sub_category_id)->pluck('title', 'id')->toArray();
        
        return view('sub_sub_categories.edit', compact('category', 'subCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubSubCategoryRequest $request, $id)
    {
        try {
            $category = SubSubCategory::findOrFail($id);
            $category->update([
                'sub_category_id' => $request->category_id,
                'title' => $request->title,
                'is_active' => $request->is_active ?? 0,
            ]);
            return redirect()->route('sub-sub-categories.index')->withMessage('Successfully Updated');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = SubSubCategory::findOrFail($id);
            $category->delete();
            return redirect()->route('sub-sub-categories.index')->withMessage('Successfully Deleted');
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
