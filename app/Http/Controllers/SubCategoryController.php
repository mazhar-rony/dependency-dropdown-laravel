<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\Http\Requests\SubCategoryRequest;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subCategories = SubCategory::with('category')->latest()->paginate(10);
        return view('sub_categories.index', compact('subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sub_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request)
    {
        try {
            SubCategory::create([
                'category_id' => $request->category_id,
                'title' => $request->title,
                'is_active' => $request->is_active ?? 0,
            ]);
            return redirect()->route('sub-categories.index')->withMessage('Successfully Created');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = SubCategory::findOrFail($id);
        return view('sub_categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubCategoryRequest $request, $id)
    {
        try {
            $category = SubCategory::findOrFail($id);
            $category->update([
                'category_id' => $request->category_id,
                'title' => $request->title,
                'is_active' => $request->is_active ?? 0,
            ]);
            return redirect()->route('sub-categories.index')->withMessage('Successfully Updated');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = SubCategory::findOrFail($id);
            $category->delete();
            return redirect()->route('sub-categories.index')->withMessage('Successfully Deleted');
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function getSubSubCategories(SubCategory $subCategory)
    {
        try {

            return response()->json(
                [
                    'status' => true,
                    'data' => $subCategory->subSubCategories,
                    'message' => 'Success !'
                ]
            );
        } catch (QueryException | Exception $e) {

            Log::error($e->getMessage());

            return response()->json(
                [
                    'status' => false,
                    'data' => [],
                    'message' => 'Somethings went wrong'
                ],
                500
            );
        }
    }
}
