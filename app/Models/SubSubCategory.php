<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
    use HasFactory;

    protected $fillable = ['sub_category_id', 'title', 'is_active'];

    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function subSubCategories()
    {
        return $this->hasMany(Product::class);
    }

}
