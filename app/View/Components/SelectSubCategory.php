<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectSubCategory extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $id;
    public $name;
    public $options;
    public $selected;
    public $categories;

    public function __construct($id = null, $name = null, $selected = null, $categories = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->selected = $selected;
        $this->categories = $categories;
        $this->options = $this->getOptions();
    }

    public function getOptions()
    {
        $options = [];
        // dd($this->categories);
        foreach ($this->categories as $category){
            $options[] = ['id' => $category['id'], 'value' => $category['title']];
        }

        return $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.select');
    }
}
