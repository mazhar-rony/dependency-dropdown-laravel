<?php

namespace App\View\Components;

use App\Models\Category;
use Illuminate\View\Component;

class SelectCategory extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $id;
    public $name;
    public $options;
    public $selected;

    public function __construct($id = null, $name = null, $selected = null)
    {

        $this->id = $id;
        $this->name = $name;
        $this->options = $this->getOptions();
        $this->selected = $selected;
    }

    public function getOptions()
    {
        $options = [];

        foreach (Category::all() as $category){
            $options[] = ['id' => $category->id, 'value' => $category->title];
        }

        return $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.select');
    }
}
