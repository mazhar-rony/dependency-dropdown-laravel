<x-layout.master>

    <x-slot:title>
        Products
    </x-slot:title>

    <x-slot:pageTitle>
        Products
    </x-slot:pageTitle>

    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Products</h3>
                            <x-utilities.link-new class="mt-2" href="{{ route('products.create') }}"
                                style="float:right" />
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if ($errors->any())
                                <x-alerts.errors />
                            @endif

                            <x-alerts.message type="success" :message="session('message')" />

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th>Sub Sub Category</th>
                                        <th>Product Title</th>
                                        <th style="white-space:nowrap; text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $product->subSubCategory->subCategory->category->title }}</td>
                                            <td>{{ $product->subSubCategory->subCategory->title }}</td>
                                            <td>{{ $product->subSubCategory->title }}</td>
                                            <td>{{ $product->title }}</td>
                                            <td style="white-space:nowrap; text-align:center;">
                                                <x-utilities.link-show href="{{ route('products.show', $product->id) }}"
                                                    icon="fas fa-solid fa-eye" />

                                                <x-utilities.link-edit href="{{ route('products.edit', $product->id) }}"
                                                    icon="fas fa-solid fa-pen" />


                                                <form method="post"
                                                    action="{{ route('products.destroy', $product->id) }}"
                                                    style="display:inline">
                                                    @csrf
                                                    @method('delete')
                                                    <x-forms.button color="danger"
                                                        onclick="return confirm('Are you sure want to delete?')"
                                                        text="Delete" icon="fas fa-solid fa-trash" />
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            {{ $products->links() }}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

    @push('css')
    @endpush

    @push('js')
    @endpush

</x-layout.master>
