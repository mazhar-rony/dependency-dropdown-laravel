<x-layout.master>

    <x-slot:title>
        Product
    </x-slot:title>

    <x-slot:pageTitle>
        Product Create
    </x-slot:pageTitle>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Product</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('products.store') }}">
                    @csrf
                    <div class="card-body">
                        @if ($errors->any())
                            <x-alerts.errors />
                        @endif

                        <div class="form-group">
                            <x-forms.input name="title" type="text" :value="old('title')" />
                        </div>

                        <div class="form-group">
                            <x-select-category id="category_id" name="category_id" />
                        </div>

                        <div class="form-group">
                            <x-forms.select id="sub_category_id" name="sub_category_id" />
                        </div>

                        <div class="form-group">
                            <x-forms.select id="sub_sub_category_id" name="sub_sub_category_id[]" multiple="multiple" />
                        </div>

                        <div class="form-check">
                            <x-forms.checkbox name="is_active" type="checkbox" value="1" />
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <x-forms.button color="primary" class="mt-2" text="Create" />
                        <x-utilities.link-cancel class="mt-2" href="{{ route('sub-sub-categories.index') }}" />
                    </div>
                </form>
            </div>
        </div>
    </div>



    @push('css')
    @endpush

    @push('js')
        <script>
            let apiUrl = '';

            let option = '<option value="">Please Select One</option>';
            let category =  document.querySelector('#category_id');
            document.querySelector('#sub_category_id').setAttribute('disabled', 'disabled');

            category.addEventListener('change', () => {
                let category_val= category.value;
                let sub_category_id = '#sub_category_id';
                apiUrl = `/categories/${category_val}/sub-categories`;
                getSubCategories(sub_category_id, option, apiUrl);
                
            })

            let select_option = '<option value="">Please Select One</option>';
            let sub_category =  document.querySelector('#sub_category_id');
            document.querySelector('#sub_sub_category_id').setAttribute('disabled', 'disabled');

            sub_category.addEventListener('change', () => {
                let sub_category_val= sub_category.value;
                let sub_category_id = '#sub_sub_category_id';
                apiUrl = `/sub-categories/${sub_category_val}/sub-sub-categories`;
                getSubCategories(sub_category_id, select_option, apiUrl);
                
            })

            const getSubCategories = (sub_category_id, option, apiUrl) => {
                    fetch(apiUrl)
                        .then((res) => res.json())
                        .then((data) => {
                            console.log(data.data);
                            document.querySelector(sub_category_id).removeAttribute('disabled');
                            data.data.forEach((subCategory) => {
                                console.log(subCategory);
                                option += `<option value="${subCategory.id}">
                                    ${subCategory.title}</option>`;
                            })
                            document.querySelector(sub_category_id).innerHTML = option;
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }

        </script>
    @endpush

</x-layout.master>

