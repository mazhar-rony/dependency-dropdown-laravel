<x-layout.master>

    <x-slot:title>
        Product
    </x-slot:title>

    <x-slot:pageTitle>
        Product Edit
    </x-slot:pageTitle>
    {{-- @dd($subCategories) --}}
    {{-- @dd($subSubCategories) --}}
{{-- @dd($product->subSubCategory->sub_category_id) --}}
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Product Edit</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('products.update', $product->id) }}">
                    @csrf
                    @method('PATCH')
                    <div class="card-body">
                        @if ($errors->any())
                            <x-alerts.errors />
                        @endif

                        <div class="form-group">
                            <x-forms.input name="title" type="text" :value="old('title', $product->title)" />
                        </div>

                        <div class="form-group">
                            <x-select-category id="category_id" name="category_id"
                            :selected="$product->subSubCategory->subCategory->category_id" />
                        </div>

                        <div class="form-group">
                            <x-select-sub-category id="sub_category_id" name="sub_category_id" 
                            :categories="$subCategories" :selected="$product->subSubCategory->subCategory->id" />
                        </div>

                        <div class="form-group">
                            {{-- <x-forms.select id="sub_sub_category_id" name="sub_sub_category_id[]" multiple="multiple" /> --}}
                            <x-select-sub-category id="sub_sub_category_id" name="sub_sub_category_id[]" multiple="multiple"
                            :categories="$subSubCategories" :selected="$product->subSubCategory->id" />
                        </div>

                        <div class="form-check">
                            <x-forms.checkbox name="is_active" type="checkbox" value="1" 
                            checked="{{ $product->is_active ? 'checked' : '' }}" />
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <x-forms.button color="primary" class="mt-2" text="Update" />
                        <x-utilities.link-cancel class="mt-2" href="{{ route('products.index') }}" />
                    </div>
                </form>
            </div>
        </div>
    </div>



    @push('css')
    @endpush

    @push('js')
        <script>
            let apiUrl = '';

            let option = '<option value="">Please Select One</option>';
            let category =  document.querySelector('#category_id');

            category.addEventListener('change', () => {
                let category_val= category.value;
                let sub_category_id = '#sub_category_id';
                apiUrl = `/categories/${category_val}/sub-categories`;
                getSubCategories(sub_category_id, option, apiUrl);
                
            })

            let select_option = '<option value="">Please Select One</option>';
            let sub_category =  document.querySelector('#sub_category_id');

            sub_category.addEventListener('change', () => {
                let sub_category_val= sub_category.value;
                let sub_category_id = '#sub_sub_category_id';
                apiUrl = `/sub-categories/${sub_category_val}/sub-sub-categories`;
                getSubCategories(sub_category_id, select_option, apiUrl);
                
            })

            const getSubCategories = (sub_category_id, option, apiUrl) => {
                    fetch(apiUrl)
                        .then((res) => res.json())
                        .then((data) => {
                            console.log(data.data);
                            data.data.forEach((subCategory) => {
                                console.log(subCategory);
                                option += `<option value="${subCategory.id}">
                                    ${subCategory.title}</option>`;
                            })
                            document.querySelector(sub_category_id).innerHTML = option;
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }

        </script>
    @endpush

</x-layout.master>

