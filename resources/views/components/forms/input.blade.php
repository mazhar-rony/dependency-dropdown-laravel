@props(['name', 'type', 'value'])

<div class="mb-3">

<x-forms.label for="{{$name}}Input" text="{{ ucfirst($name) }}"/>     

<input name="{{ $name }}" id="{{ $name.'Input' }}" type={{ $type }} value="{{ $value }}"
{{ $attributes->merge([
    'class' => "form-control" 
    ]) }}
>

<x-forms.error name="{{$name}}"/>

</div>