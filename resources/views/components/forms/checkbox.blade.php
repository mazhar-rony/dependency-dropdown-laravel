@props(['name', 'type', 'value', 'checked' => ''])

<div class="mb-3">

    <input type="{{ $type }}" name="{{ $name }}" id="{{ $name.'Input' }}"
    {{ $attributes->merge([
        'class' => "form-check-input" 
        ]) }} value="{{ $value }}" {{ $checked }}
>
<x-forms.label for="{{$name}}Input" text="{{ ucwords( str_replace('_', ' ',$name)) }}"/>     

</div>