@props(['id', 'name', 'options' => [], 'multiple' => null, 'selected' => null])
<div class="mb-3">

    <x-forms.label for="{{$id}}" text="{{ ucwords(str_replace('_', ' ', $name)) }}" />
    <select name="{{$name}}" id="{{$id}}" {{ $attributes->merge([
    'class' => "form-control" 
    ]) }} {{ $multiple }}>
    
        <option value="">Select One</option>
        @foreach ($options as $option)
            <option value="{{ $option['id'] }}" {{ $option['id']  == $selected ? 'selected' : ''}}>{{ $option['value'] }}</option>
        @endforeach
    </select>
    <x-forms.error name="{{$name}}" />

</div>