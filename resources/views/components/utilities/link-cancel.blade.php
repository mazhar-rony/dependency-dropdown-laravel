@props(['text' => 'Cancel', 'icon' => ''])

<a {{ $attributes->merge(['class' => 'btn btn-sm btn-secondary']) }}>
   
    @if($icon)
        <i class="{{$icon}}"></i>
    @else
        {{ $text }}
    @endif

</a>
                           