@props(['text' => 'Show', 'icon' => ''])

<a {{ $attributes->merge(['class' => 'btn btn-sm btn-info']) }}>
   
    @if($icon)
        <i class="{{$icon}}"></i>
    @else
        {{ $text }}
    @endif

</a>
                           