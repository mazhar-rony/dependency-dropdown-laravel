@props(['text' => 'Create New', 'icon' => ''])

<a {{ $attributes->merge(['class' => 'btn btn-sm btn-success']) }}>
   
    @if($icon)
        <i class="{{$icon}}"></i>
    @else
        {{ $text }}
    @endif

</a>
                           