<x-layout.master>

    <x-slot:title>
        Sub Categories
    </x-slot:title>

    <x-slot:pageTitle>
        Sub Category Create
    </x-slot:pageTitle>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Sub Category Create</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('sub-categories.store') }}">
                    @csrf
                    <div class="card-body">
                        @if ($errors->any())
                            <x-alerts.errors />
                        @endif

                        <div class="form-group">
                            <x-forms.input name="title" type="text" :value="old('title')" />
                        </div>

                        <div class="form-group">
                            <x-select-category id="category_id" name="category_id" />
                        </div>

                        <div class="form-check">
                            <x-forms.checkbox name="is_active" type="checkbox" value="1" />
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <x-forms.button color="primary" class="mt-2" text="Create" />
                        <x-utilities.link-cancel class="mt-2" href="{{ route('sub-categories.index') }}" />
                    </div>
                </form>
            </div>
        </div>
    </div>



    @push('css')
    @endpush

    @push('js')
    @endpush

</x-layout.master>
