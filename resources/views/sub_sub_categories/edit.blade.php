<x-layout.master>

    <x-slot:title>
        Sub Sub Categories
    </x-slot:title>

    <x-slot:pageTitle>
        Sub Sub Category Edit
    </x-slot:pageTitle>
    {{-- @dd($subCategories) --}}
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Sub Sub Category Edit</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('sub-sub-categories.update', $category->id) }}">
                    @csrf
                    @method('PATCH')
                    <div class="card-body">
                        @if ($errors->any())
                            <x-alerts.errors />
                        @endif

                        <div class="form-group">
                            <x-forms.input name="title" type="text" :value="old('title', $category->title)" />
                        </div>

                        <div class="form-group">
                            <x-select-category id="category_id" name="category_id" :selected="$category->subCategory->category_id" />
                        </div>

                        <div class="form-group">
                            <x-select-sub-category id="sub_category_id" name="sub_category_id" 
                            :categories="$subCategories" :selected="$category->sub_category_id" />
                        </div>

                        <div class="form-check">
                            <x-forms.checkbox name="is_active" type="checkbox" value="1"
                              :options = "options"  checked="{{ $category->is_active ? 'checked' : '' }}" />
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <x-forms.button color="primary" class="mt-2" text="Update" />
                        <x-utilities.link-cancel class="mt-2" href="{{ route('sub-sub-categories.index') }}" />
                    </div>
                </form>
            </div>
        </div>
    </div>



    @push('css')
    @endpush

    @push('js')
    <script>
        let option = '<option value="">Please Select One</option>';
        let category =  document.querySelector('#category_id');

        category.addEventListener('change', () => {
            let category_val= category.value;
            option = '<option value="">Please Select One</option>';
            getSubCategories(category_val);
            
        })

        const getSubCategories = (categoryId) => {
                const apiUrl = `/categories/${categoryId}/sub-categories`;
                fetch(apiUrl)
                    .then((res) => res.json())
                    .then((data) => {
                        console.log(data.data);
                        data.data.forEach((subCategory) => {
                            console.log(subCategory);
                            option += `<option value="${subCategory.id}">
                                ${subCategory.title}</option>`;
                        })
                        document.querySelector('#sub_category_id').innerHTML = option;
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            }
    </script>
    @endpush

</x-layout.master>
