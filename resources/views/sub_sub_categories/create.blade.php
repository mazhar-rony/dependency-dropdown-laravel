<x-layout.master>

    <x-slot:title>
        Sub Sub Categories
    </x-slot:title>

    <x-slot:pageTitle>
        Sub Sub Category Create
    </x-slot:pageTitle>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Sub Sub Category Create</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('sub-sub-categories.store') }}">
                    @csrf
                    <div class="card-body">
                        @if ($errors->any())
                            <x-alerts.errors />
                        @endif

                        <div class="form-group">
                            <x-forms.input name="title" type="text" :value="old('title')" />
                        </div>

                        <div class="form-group">
                            <x-select-category id="category_id" name="category_id" />
                        </div>

                        <div class="form-group">
                            <x-forms.select id="sub_category_id" name="sub_category_id" />
                        </div>

                        <div class="form-check">
                            <x-forms.checkbox name="is_active" type="checkbox" value="1" />
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <x-forms.button color="primary" class="mt-2" text="Create" />
                        <x-utilities.link-cancel class="mt-2" href="{{ route('sub-sub-categories.index') }}" />
                    </div>
                </form>
            </div>
        </div>
    </div>



    @push('css')
    @endpush

    @push('js')
        <script>
            let option = '<option value="">Please Select One</option>';
            let category =  document.querySelector('#category_id');
            document.querySelector('#sub_category_id').setAttribute('disabled', 'disabled');

            category.addEventListener('change', () => {
                let category_val= category.value;
                option = '<option value="">Please Select One</option>';
                getSubCategories(category_val);
                
            })

            const getSubCategories = (categoryId) => {
                    const apiUrl = `/categories/${categoryId}/sub-categories`;
                    fetch(apiUrl)
                        .then((res) => res.json())
                        .then((data) => {
                            console.log(data.data);
                            document.querySelector('#sub_category_id').removeAttribute('disabled');
                            data.data.forEach((subCategory) => {
                                console.log(subCategory);
                                option += `<option value="${subCategory.id}">
                                    ${subCategory.title}</option>`;
                            })
                            document.querySelector('#sub_category_id').innerHTML = option;
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }
        </script>
    @endpush

</x-layout.master>
